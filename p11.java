import java.util.*;

public class p11 {
	public static void main(String[] args) {
		int[][] mass = {
			{8, 1, 6},
			{3, 5, 7},
			{4, 9, 2}
		};
		
		final int n = mass.length;
		if(n!=mass[0].length) {
			System.out.println("ERR");
			return;
		}
		
		boolean control_sum_initiated = false;
		int control_sum = 0;
		for(int x=0; x<n; ++x) {
			int sum_x = 0;
			int sum_y = 0;
			for(int y=0; y<n; ++y) {
				sum_y+=mass[x][y];
				sum_x+=mass[y][x];
			}
			if(!control_sum_initiated) {
				control_sum = sum_x;
				control_sum_initiated = true;
			}
			else {
				if(control_sum != sum_x || control_sum != sum_y) {
					System.out.println("NO");
					return;
				}
			}
		}
		int sum_d1 = 0, sum_d2 = 0;
		for(int i=0; i<n; ++i) {
			sum_d1 += mass[i][i];
			sum_d2 += mass[n-i-1][n-i-1];
		}
		if(control_sum != sum_d1 || control_sum != sum_d2) {
			System.out.println("NO");
			return;
		}
		System.out.println("YES");
	}
}
import java.util.*;

public class p5 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Print N: ");
		int n = sc.nextInt();
		int ans=1;
		for(int i=2; i<=n; i+=2) {
			ans*=i;
		}
		System.out.println("N!! is " + ans);
	}
}
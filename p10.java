import java.util.*;

public class p10 {
	public static void main(String[] args) {
		final int n = 8;
		int[][] mass = new int [n][n];
		for(int x=0; x<n; ++x) {
			for(int y=0; y<n; ++y) {
				if(x>=n/2 && y<n/2 || x<n/2 && y>=n/2) {
					mass[x][y] = n-1;
				}
				else if(x<n/2 && y<n/2) {
					mass[x][y] = x+y;
				}
				else {
					if(x<y) {
						mass[x][y] = n/2+y;
					}
					else {
						mass[x][y] = n/2+x;
					}
				}
				if(mass[x][y]<10) {
					System.out.print(" ");
				}
				System.out.print(" " + mass[x][y]);
				
			}
			System.out.println();
		}
	}
}
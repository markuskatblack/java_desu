import java.util.*;

public class p13 {
	public static void main(String[] args) {
		
		// input and matrix initiating...
		Scanner sc = new Scanner(System.in);
		System.out.print("Print matrix size: ");
		int mas_size = sc.nextInt();
		double [][] mas = new double [mas_size][mas_size];
		System.out.println("Created matrix with " + mas_size + " weight and " 
			+ mas_size + " height.");
		System.out.println("Now you have to print each value in matrix.");
		System.out.println("Numbers will be real.");
		for(int x=0; x<mas_size; ++x) {
			for(int y=0; y<mas_size; ++y) {
				System.out.print("Print element in position " + (x+1) + ", " 
					+ (y+1) + " : ");
				mas[x][y] = sc.nextDouble();
			}
		}

		System.out.print("Calculating... ");
		
		// calculates...
		final double eps = 1e-6;
		double[][] answer = mas;
		for(long k=2; max(answer)>=eps; ++k) {
			
			if(k<0) { // out of range
				System.out.println("too long. Memory error.");
				return;
			}

			answer = addition(answer, division(mas = multipl(mas), fact(k)));
		}
		
		// output
		System.out.println("done.");
		System.out.println("Now matrix looks like");
		for(int x=0; x<mas_size; ++x) {
			for(int y=0; y<mas_size; ++y) {
				System.out.print(answer[x][y] + " ");
			}
			System.out.println();
		}
	}
	
	public static double[][] addition(double[][] a, double[][] b) {
		final int size = a.length;
		for(int x=0; x<size; ++x) {
			for(int y=0; y<size; ++y) {
				a[x][y]+=b[x][y];
			}
		}
		return a;
	}
	
	public static double[][] division(double[][] a, long n) {
		final int size = a.length;
		for(int x=0; x<size; ++x) {
			for(int y=0; y<size; ++y) {
				a[x][y]/=n;
			}
		}
		return a;
	}
	
	public static double[][] multipl(double[][] a) {
		final int size = a.length;
		double [][] ans = new double[size][size];
		for(int x=0; x<size; ++x) {
			for(int y=0; y<size; ++y) {
				ans[x][y] += a[x][y] * a[y][x];
			}
		}
		return ans;
	}
	
	public static long fact(long n) {
		long ans = 1;
		for(long i=2; i<=n; ++i) {
			ans*=i;
		}
		return ans;
	}
	
	public static double max(double[][] a) {
		final int size = a.length;
		double ans = a[0][0];
		for(int x=0; x<size; ++x) {
			for(int y=0; y<size; ++y) {
				if(ans < a[x][y]) {
					ans = a[x][y];
				}
			}
		}
		return ans;
	}
}
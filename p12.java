import java.util.*;

public class p12 {
	public static void main(String[] args) {
		final int n = 4;
		int[][] mass = new int [n][n];
		
		for(int i=0, k=1, j, p=0; i<n*n; ++k, ++i) {
			for (j=k-1; j<n-k+1; ++j) {
				mass[k-1][j] = ++p;
			}
			for (j=k; j<n-k+1; ++j) {
				mass[j][n-k] = ++p;
			}
			for (j=n-k-1; j>=k-1; --j) {
				mass[n-k][j] = ++p;
			}
			for (j=n-k-1; j>=k; --j) {
				mass[j][k-1] = ++p;
			}
		}
 
		for(int y=0; y<n; ++y) {
			for(int x=0; x<n; ++x) {
				if(mass[x][y]<10) {
					System.out.print(" ");
				}
				System.out.print(" " + mass[x][y]);				
			}
			System.out.println();
		}
		
	}
}
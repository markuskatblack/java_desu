import java.util.*;

public class p8 {
	public static void main(String args[]) {
		final int mass_size = 9;
		
		// here - 2 - prime, and after it - 3 - non even number
		// then answer will be NO
		int[] mass = { 1, 2, 3, 4, 5, 6, 7, 8};
		
		for(int i=0; i<mass_size-1; ++i) {
			boolean flag = true;
			
			for(int j=2; j<mass[i]; ++j) {
				if(mass[i]%j==0) {
					flag = false;
				}
			}
			
			if(flag) {
				if(mass[i+1]%2!=0) {
					System.out.println("NO");
					return;
				}
			}
		}
		System.out.println("YES");
	}
}
import java.util.*;

public class p6 {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Print X: ");
		int x = sc.nextInt();
		
		int max=0;
		int length=0;
		
		System.out.print(x + " -> ");
		
		do {
			if(x%2==0) {
				x/=2;
			}
			else {
				x=x*3+1;
			}
			length++;
			if(max<x) {
				max = x;
			}
			System.out.print(x + " ");
		} while(x!=1);
		//System.out.println();
		System.out.println("\nLength is " + length + " and max value is " + max);
	}
}